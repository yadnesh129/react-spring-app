package com.example.demo.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Product;
import com.example.demo.service.ProductService;

@RestController
public class ProductController {
	
	@Autowired
	private ProductService productService;

	
	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping("/getAll")
	public ResponseEntity<List<Product>> test() {
		List<Product> productList=productService.getAll();
		return new ResponseEntity<List<Product>> (productList,HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping("/getProduct/id/{prodId}")
	public ResponseEntity<Product> test(@PathVariable("prodId") int prodId) {
		Product productList=productService.getProduct((long) prodId);
		return new ResponseEntity<Product> (productList,HttpStatus.OK);
	}
	
	

}
