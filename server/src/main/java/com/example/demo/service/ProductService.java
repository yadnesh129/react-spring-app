package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Product;
import com.example.demo.repository.ProductRepository;

@Service
public class ProductService {
	
	@Autowired
	private ProductRepository productRepository;
	
	public void save(Product product) {
		productRepository.save(product);
	}

	public void delete(Product product) {
		productRepository.deleteById(product.getId());
	}
	
	public Product getProduct(Long id) {
		return productRepository.findById(id).get();
	}
	
	public List<Product> getAll() {
		return productRepository.findAll();
	}
}
